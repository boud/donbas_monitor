# makefile for donbas_monitor package
# (C) 2022 B. Roukema GPL-3+

# This project is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This project is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this project.  If not, see <https://www.gnu.org/licenses/>.


all: outputs/violations.out outputs/donbas.eps

analyse: outputs/violations.out

figs: outputs/donbas.eps

# The analysis needs the pdf files in input_pdfs/ . See
# README.md and the files ceasefires[01].pdfs .
outputs/violations.out: get_counts.sh
	./get_counts.sh > violations.out.full 2>&1 # very verbose
	cat data.copyright.template > outputs/violations.out  # bash syntax
# pure data file, apart from the header:
	printf "#\n#Col 1: Date in format YYYY-mm-dd\n" >> outputs/violations.out
	printf "#Col 2: Date in days since 1 Jan 2018\n" >> outputs/violations.out
	printf "#Col 3: Number of violations\n#\n" >> outputs/violations.out
	grep "^violations" violations.out.full | sed -e 's/violations://' > outputs/v.tmp
# merge automatically and manually extracted violations counts, and append to violations.out
	grep -hv "^#" violations_manual outputs/v.tmp |sort -n -k1,1 >> outputs/violations.out


# This plot is made with 'graph' from 'plotutils'; any other
# standard plotting package may be used instead.
outputs/donbas.eps:  get_counts.sh outputs/violations.out
	N_MAX=5000
	grep -v "^ *#" outputs/violations.out | \
	  awk '{print $$2,$$3}' | \
	  graph -Tps -y 0 ${N_MAX} -X "days since 1 Jan 2018" -Y 'OSCE SMM violation count' > outputs/donbas.eps
	grep -v "^ *#" outputs/violations.out | \
	  awk '{print $$2,$$3}' | \
	  graph -Tpng -y 0 ${N_MAX} --bitmap-size 2000x2000 --font-size 0.04 --line-width 0.002 --frame-line-width 0.002 -X "\fBdays since 1 Jan 2018" -Y '\fBOSCE SMM violation count' > outputs/donbas.png

clean:
	rm -fv outputs/violations.out xx?? yy?? zz??

.PHONY: analyse figs clean
