#!/bin/bash

# get_counts - extract violations counts from OSCE SMM for Donbas
# (C) 2022 B. Roukema GPL-3+

# This project is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This project is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this project.  If not, see <https://www.gnu.org/licenses/>.

## TODO
##
## Bug: Space between 2nd and 3rd parts in Method III sometimes right, sometimes wrong.
##
## As of the first commit of 2022-03-13, the addition of a space after
## the (Recorded|Heard) column in method III fixes the parsing for some
## of the very recent pdfs, but modifies quite a few of the analyses of
## the previous pdfs: some estimates increase, some decrease.
## /files/e/2/table_ceasefire--2019-07-29.pdf (for date 2019-07-28) is
## analysed wrongly with this commit: 366 drops to 237.


pdf_test=table_ceasefire--2022-02-09.pdf
#pdf_test=table_ceasefire--2019-05-01.pdf
#pdf_test=table_ceasefire--2019-10-30.pdf.1  # several lines are missed using methodII for this case => underestimate
#pdf_test=table_ceasefire--2020-07-28.pdf.2 # this has -9- type page numbers in the No. column
#pdf_test=375625.pdf

#DEBUG=yes
DEBUG=no

TMP1=$(mktemp /tmp/tmp.ceasefire_col_XXXXXXXXX)
echo ${TMP1}

# Is this an English-language ceasefire table?
function is_en_ceasefire {
    pdf=$1
    pdftotext -layout ${pdf} ${TMP1}
    if(grep "Table of ceasefire violations" ${TMP1}); then
        # For the moment, the older, e.g. 2017, style pdfs without a
        # "No." column will be excluded, since they have a more
        # prose-style format and need different parsing.
        if(grep "No\." ${TMP1}); then
            valid_pdf=yes
        else
            valid_pdf=no
        fi
    else
        valid_pdf=no
    fi
}


# Get the column 'No.' from the pdf file $1.
# This fails on the earlier pdfs, in which only one page
# has a header column with 'No.' written on it.
function get_column_No {
    pdf=$1
    pdftotext -layout ${pdf} ${TMP1}

    csplit ${TMP1} "//"

    n_old=0
    for XX in xx00 xx01; do
        csplit --prefix=yy ${XX} "/No\./2"
        csplit --prefix=zz yy01 "/The table only/" || cp -pv yy01 zz00

        # Find first header line and identify the character positions with "No."
        line=$(grep "SMM *position.*No\." ${XX} |head -n 1)
        if(echo "${line}" | grep "No."); then
            regex=$(echo "${line}"|sed -e 's/[^N]/\./g' -e "s/N..\(.*\)\'/\\\(\.\.\.\\\)/")
            echo regex="${regex}"
            n_violations=$(grep "${regex}" zz00 | sed -e "s/${regex}.*/\1/" | \
                               sed -e "s/^\([0-9][0-9]*\).*\'/\1/" | \
                               grep -v "^ *$" | \
                               awk -v nv=${n_old} '{n+=$1} END {print n+nv}')
            n_old=${n_violations}
        fi
    done
}

# Get the numerical column immediately after "Recorded" or "Heard"
# from the pdf file $1. This fails when a spreadsheet box is
# spread across several lines, and "Heard" is at the bottom of its
# box while the number is in the middle line of its box.
function get_column_methodII {
    pdf=$1
    pdftotext -layout ${pdf} ${TMP1}

    n_violations=$(cat ${TMP1} | \
                       egrep "(Recorded|Heard) *([0-9][0-9]*)" | \
                       sed -e "s/^.*\(Heard\|Recorded\) *\([0-9][0-9]*\) *.*\'/\2/" | \
                       awk '{n+=$1} END {print n}')
    if [ "x${n_violations}" = "x" ]; then
        n_violations=0 # means none were detected in the file
    fi
}

# Get the numerical column immediately after "Recorded" or "Heard" based
# on matching exact horizontal positions.
function get_column_methodIII {
    pdf=$1
    pdftotext -layout ${pdf} ${TMP1}

    # Split the pages
    rm -fv xx??
    csplit ${TMP1} "//" '{*}'

    pages=$(for XX in $(ls xx??); do
                n_lines=$(cat ${XX} |wc -l)
                if [ ${n_lines} -gt 2 ]; then
                    printf "${XX} "
                fi
            done)
    echo pages=${pages}

    n_old=0
    for XX in ${pages}; do
        sed -e 's///g' ${XX} > xx99 # avoid the 1-char shift that ^L can induce

        # First try 'Recorded', second try 'Heard', to create a
        # regular expression for the correct character columns.
        line=$(grep " Recorded " xx99 |head -n 1)
        if !(echo "${line}" | grep "Recorded"); then
            line=$(grep " Heard " xx99 |head -n 1)
        fi
        echo line="${line}"
        if(echo "${line}" | egrep "(Recorded|Heard)"); then

            # Store (1) the part before the Recorded|Heard column; (2)
            # the column itself; and (3) the numerical column
            # afterwards; in all cases preserving the exact character
            # sequences.
            first_part=$(echo "${line}" |sed -e "s/^\(.*\)\(Recorded\|Heard\).*\'/\1/")
            first_part_bis=$(echo "${first_part}" | sed -e 's/./\./g')

            second_part=$(echo "${line}" |sed -e "s/^.*\(\(Recorded\|Heard\) \)\( *[0-9]* *\) [^ ].*\'/\1/")
            second_part_bis=$(echo "${second_part}" | sed -e 's/./\./g')
            if [ "x${DEBUG}" = "xyes" ]; then
                printf "second_part=_____${second_part}______\n"
            fi

            third_part=$(echo "${line}" |sed -e "s/^.*\(Recorded\|Heard\) \( *[0-9]* *\) [^ ].*\'/\2/")
            if [ "x${DEBUG}" = "xyes" ]; then
                printf "third_part=_____${third_part}______\n"
            fi
            third_part_bis=$(echo "${third_part}" | sed -e 's/./\./g')

            # Shorten third part if it's suspiciously long.
            MAX_3RD=10
            len_third_part=$(echo "${third_part_bis}" | awk '{print length($1)}')
            if [ "${len_third_part}" -gt "${MAX_3RD}" ]; then
                third_part_bis=".........."
            fi

            # Check the length of the first two parts so that only
            # long enough lines are analysed.
            min_len=$(echo "${first_part_bis}${second_part_bis}" |awk '{print length($1)}')
            printf "min_len=${min_len}\n"
            regex="^${first_part_bis}${second_part_bis}\\\(${third_part_bis}\\\)"
            printf "regex=\n${regex}\n___\n" # not actually used; just check
            #grep "${regex}" xx99

            csplit --prefix=zz xx99 "/The table only/" || cp -pv xx99 zz00

            if [ "x${DEBUG}" = "xyes" ]; then
                printf "<<<<<<DEBUG\n"
                awk -v l=${min_len} '{if(length($0) >= l) print $0}' zz00 | \
                    egrep -v "(No\.|Table of)" | \
                    grep -v "\-[0-9]\-" | \
                               sed -e "s/\'/              /"
                printf ">>>>>>DEBUG\n"
            fi

            # Page numbers, e.g. '-9-', have to be excluded.
            #
            # Spaces are added because sometimes a line ends with the
            # count rather than filling the column completely.

            # Try to parse the column and check for its credibility.
            n_violations_list=$(awk -v l=${min_len} '{if(length($0) >= l) print $0}' zz00 | \
                               egrep -v "(No\.|Table of)" | \
                               grep -v "\-[0-9]\-" | \
                               sed -e "s/\'/              /" | \
                               sed -e "s/^${first_part_bis}${second_part_bis}\(${third_part_bis}\).*\'/\1/" | \
                               grep -v "^ *$")
            echo n_violations_list=$(echo ${n_violations_list}| tr '\n' ' '); echo ""
            # TODO: The list 'n_violations_list' could be parsed to allow cases
            # where a file or page has particular characteristics (tabs? coding?)
            # that yield a text file with huge numbers of spaces - differing
            # from the expected result.

            n_violations=$(awk -v l=${min_len} '{if(length($0) >= l) print $0}' zz00 | \
                               egrep -v "(No\.|Table of)" | \
                               grep -v "\-[0-9]\-" | \
                               sed -e "s/\'/              /" | \
                               sed -e "s/^${first_part_bis}${second_part_bis}\(${third_part_bis}\).*\'/\1/" | \
                               grep -v "^ *$" | \
                               awk -v o=${n_old} '{n+=$1} END {print o+n}')
            n_old=${n_violations}
        fi
        printf "${XX} n_violations=${n_violations}\n"
    done

    if [ "x${n_violations}" = "x" ]; then
        n_violations=0 # means none were detected in the file
    fi
}


# get the date from the pdf file $1
function get_date {
    pdf=$1
    pdftotext -layout ${pdf} ${TMP1}

    date1=$(grep -i "Table of ceasefire violations.* of" ${TMP1} | \
                sed -e "s/^.*Table.* of \([0-9][0-9]* *[JASONFMD][a-z]* 2[0-9][0-9][0-9]\).*\'/\1/")
    date_iso=$(date +"%Y-%m-%d" --date="${date1}")
    date_dayofyear=$(date +"%j" --date="${date1}" | sed -e 's/^0*//')
    date_year=$(date +"%Y" --date="${date1}")
    case ${date_year} in
        #2016)
        #    date_days=${date_dayofyear}
        #    ;;
        #2017)
        #    date_days=$((365+1+${date_dayofyear})) # +1 for the leap year 2020
        #    ;;
        2018)
            date_days=$((0+${date_dayofyear}))
            ;;
        2019)
            date_days=$((1*365+${date_dayofyear}))
            ;;
        2020)
            date_days=$((2*365+${date_dayofyear}))
            ;;
        2021)
            date_days=$((3*365+1+${date_dayofyear})) # +1 for the leap year 2020
            ;;
        2022)
            date_days=$((4*365+1+${date_dayofyear}))
            ;;
        *)
            printf "year ${date_year} not yet handled.\n"
            date_days=$((10000+${date_dayofyear}))
            ;;
    esac
}

if [ "x${DEBUG}" = "xyes" ]; then
    pdf_list=${pdf_test}
else
    pdf_list=$(ls input_pdfs/*.pdf input_pdfs/*.pdf.*)
fi

for pdf in ${pdf_list}; do

    echo pdf=${pdf}

    is_en_ceasefire ${pdf}

    if [ "x${valid_pdf}" = "xyes" ] ; then
        printf "valid_pdf: ${pdf}\n"

        get_column_methodIII ${pdf}

        get_date ${pdf}

        printf "violations: ${date_iso} ${date_days} ${n_violations}\n"
    else
        printf "${pdf} does not seem to be an English language ceasefire table\n"
        pdfinfo ${pdf} | grep Title:
    fi

done


rm -fv ${TMP1}
