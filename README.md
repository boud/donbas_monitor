Donbas monitor
==============

This package, 'donbas_monitor', aims to provide some help in
extracting ceasefire violations counts in the war in Donbas [1] from
the data files provided by the reports of the OSCE Special Monitoring
Mission (OSCE SMM) [2].

(C) 2022 B. Roukema GPL-3+

This project is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This project is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this project.  If not, see <https://www.gnu.org/licenses/>.

[1] https://en.wikipedia.org/wiki/War_in_Donbas
[2] https://en.wikipedia.org/wiki/OSCE_Special_Monitoring_Mission_to_Ukraine


Files and hints
===============

DATA: The files 'ceasefire0.pdfs' and 'ceasefire1.pdfs' are provided
for convenience with a list of the URLs to the files with the
ceasefire violation reports; the base domain name https://www.osce.org
is to be added by the user. Please respect the limitations of the
server and allow an appropriate delay (at least a few seconds) between
downloading individual files - there are many of them.

The ceasefire0.pdfs file lists only English-language files;
ceasefire1.pdfs lists a mix of English, Ukrainian and Russian
language files.

Use a downloader such as 'wget' that by default gives separate names
to identically named files; the multiple versions of a given
filename appear to represent the different languages.

These two lists were created from the publicly provided
html files available at the OSCE website.


RESOURCE USAGE: The pdf source files listed in ceasefire[01].pdfs
will take about 1.2 Gb of disk space; downloading at intervals of
many seconds will take many hours. This is wasteful; ideally,
OSCE SMM should provide machine-readable csv files, requiring
something like 3-10 times less disk space and download volumes.


ANALYSIS: The current (2022-01-29) versions of the files appear
to be spreadsheet type source files converted to human-readable
pdf format. Parsing these requires a few simple hacks that work
on many of the more recent English-language files, and some of
the older files.

The script 'get_counts.sh' assumes that you have already downloaded
the pdf files listed in 'ceasefire[01].pdfs'.


PLOTS: A current set of outputs is provided in 'outputs/'.
You can make your own plots using columns 2 and 3 'outputs/violations.out',
or use columns 1 and 3 if you have a fancy way of showing dates.
Ignore lines starting with a hash '#'.
![Donbas ceasefire violations since 1 Jan 2018!](outputs/donbas.png "Donbas ceasefire violations 2018-2022-01")


CAVEATS:

* the reports appear to be produced slightly more often than once every second day;
* the variation in file formatting currently leads to some *parsing errors*; see CONTRIBUTE if you have an improved or better analysis script;
* some periods clearly have missing source pdfs;
* most files from before 2018 are missing from these lists; a few are listed in 'ceasefire[01].pdfs', but parsing them would require a different algorithm, and these are currently ignored;
* 'recorded' and 'heard' violations are currently not distinguished
* to match dates to day counts, look through 'violations.out'


CONTRIBUTE:

Fork this git repository, propose a merge request, and/or discuss issues or bugs as 'Issues' at https://codeber.org/boud/donbas_monitor .
